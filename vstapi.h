#ifndef VSTAPI_H
#define VSTAPI_H

#include <string>

namespace VstAPI {

int getUniqueID(AEffect *eff);

int getVersion(AEffect *eff);

void saveProgram(AEffect *eff,
  const std::string &inChunk, const std::string &inProgramName,
  std::string &outProgram);

bool loadProgram(AEffect *eff,
  const std::string &inProgram,
  std::string &outChunk, std::string &outProgramName, int &outFxID, int &outFxVersion);

void saveBank(AEffect *eff,
  const std::string &inChunk,
  std::string &outBank);

bool loadBank(AEffect *eff,
  const std::string &inBank,
  std::string &outChunk, int &outFxID, int &outFxVersion);

} // namespace VstAPI

#endif // VSTAPI_H
