
#include "aeffectx.h"
#include "vstfxstore.h"
#include <string>
#include <cstddef>
#include <cstring>

namespace VstAPI {

int getUniqueID(AEffect *eff)
{
  return eff->uniqueID;
}

int getVersion(AEffect *eff)
{
  return eff->version;
}

void saveProgram(AEffect *eff,
  const std::string &inChunk, const std::string &inProgramName,
  std::string &outProgram)
{
  fxProgram *program = 0;
  unsigned headerSize = sizeof(*program) - sizeof(program->content);
  outProgram.resize(headerSize + inChunk.size());
  std::fill(outProgram.begin(), outProgram.end(), '\0');
  program = (fxProgram *) outProgram.data();
  program->chunkMagic = __bswap_32(cMagic);
  program->byteSize = __bswap_32(outProgram.size() - 8);
  program->fxMagic = __bswap_32(chunkPresetMagic);
  program->version = __bswap_32(1);
  program->fxID = __bswap_32(eff->uniqueID);
  program->fxVersion = __bswap_32(eff->version);
  program->numParams = __bswap_32(eff->numParams);
  strncpy(program->prgName, inProgramName.c_str(), sizeof(program->prgName));
  program->prgName[sizeof(program->prgName) - 1] = '\0';
  std::memcpy(&program->content, inChunk.data(), inChunk.size());
}

bool loadProgram(AEffect *eff,
  const std::string &inProgram,
  std::string &outChunk, std::string &outProgramName, int &outFxID, int &outFxVersion)
{
  fxProgram *program = (fxProgram *) inProgram.data();
  unsigned headerSize = sizeof(*program) - sizeof(program->content);
  if (inProgram.size() < headerSize) {
    return false;
  }
  outChunk.assign((char *) &program->content, inProgram.size() - offsetof(fxProgram, content));
  char prgName[sizeof(program->prgName) + 1];
  std::memcpy(prgName, program->prgName, sizeof(program->prgName));
  prgName[sizeof(program->prgName)] = '\0';
  outProgramName.assign(prgName);
  outFxID = __bswap_32(program->fxID);
  outFxVersion = __bswap_32(program->fxVersion);
  return true;
}

void saveBank(AEffect *eff,
  const std::string &inChunk,
  std::string &outBank)
{
  fxBank *bank = 0;
  unsigned headerSize = sizeof(*bank) - sizeof(bank->content);
  outBank.resize(headerSize + inChunk.size());
  std::fill(outBank.begin(), outBank.end(), '\0');
  bank = (fxBank *) outBank.data();
  bank->chunkMagic = __bswap_32(cMagic);
  bank->byteSize = __bswap_32(outBank.size() - 8);
  bank->fxMagic = __bswap_32(chunkBankMagic);
  bank->version = __bswap_32(1);
  bank->fxID = __bswap_32(eff->uniqueID);
  bank->fxVersion = __bswap_32(eff->version);
  bank->numPrograms = __bswap_32(eff->numPrograms);
  std::memcpy(&bank->content, inChunk.data(), inChunk.size());
}

bool loadBank(AEffect *eff,
  const std::string &inBank,
  std::string &outChunk, int &outFxID, int &outFxVersion)
{
  fxBank *bank = (fxBank *) inBank.data();
  unsigned headerSize = sizeof(*bank) - sizeof(bank->content);
  if (inBank.size() < headerSize) {
    return false;
  }
  outChunk.assign((char *) &bank->content, inBank.size() - offsetof(fxBank, content));
  outFxID = __bswap_32(bank->fxID);
  outFxVersion = __bswap_32(bank->fxVersion);
  return true;
}

} // namespace VstAPI
